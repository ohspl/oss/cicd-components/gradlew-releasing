pluginManagement {
	repositories {
		maven {
			// name: O.W.S
			// url : https://gitlab.com/ohspl
			url = uri("https://gitlab.com/api/v4/groups/11708027/-/packages/maven")
			credentials(HttpHeaderCredentials::class) {
				val ciJobToken = System.getenv("CI_JOB_TOKEN")
				val gitlabAccessToken: String? by settings
				name = ciJobToken?.let { "Job-Token" } ?: "Private-Token"
				value = ciJobToken ?: gitlabAccessToken
			}
			authentication {
				create<HttpHeaderAuthentication>("header")
			}
			content {
				includeGroupByRegex("org\\.gitlab\\.bullshit\\.devops\\.toolchain.*")
			}
		}
		gradlePluginPortal()
	}
}

rootProject.name = "gradlew-releasing"

include("demo-spring")

plugins {
	id("org.gitlab.bullshit.devops.toolchain.cicd-gradle-plugin") version "0.3.18"
}

// region nyx - releasing
configure<com.mooltiverse.oss.nyx.gradle.NyxExtension> {
	substitutions {
//		if (System.getenv("CI") == "true") {
//			enabled.set(listOf("gitlab_component_java", "gitlab_component_springboot"))
//		}
		items.create("gitlab_component_java") {
			files.set("README.md")
			match.set("java@(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)(?:-((?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\\.(?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\\+([0-9a-zA-Z-]+(?:\\.[0-9a-zA-Z-]+)*))?")
			replace.set("java@{{version}}")
		}
		items.create("gitlab_component_springboot") {
			files.set("README.md")
			match.set("springboot@(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)(?:-((?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\\.(?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\\+([0-9a-zA-Z-]+(?:\\.[0-9a-zA-Z-]+)*))?")
			replace.set("springboot@{{version}}")
		}
	}
} // endregion
