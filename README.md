# Gradle wrapper releasing with Nyx

This repository contains components for building, testing, publishing and releasing Java or Spring Boot applications.

Ensure your Gradle project uses either the `maven-publish` or `org.springframework.boot` Gradle plugin,
and includes a release task for creating git tags.

For managing version numbers and creating tags and releases, [Nyx](https://mooltiverse.github.io/nyx/) is highly recommended.
It's also utilized within this repository to test and release the components itself with a [demo-spring](demo-spring/) application.

Additionally, this project applies a custom, non-public Gradle plugin `org.gitlab.bullshit.devops.toolchain.cicd-gradle-plugin` to
configure various Gradle settings, including specifying Java source compatibility versions,
and handling authentication for container registries and artifact repositories and defining release types and so on with Nyx.

## Usage

This template can be used as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration).

### Usage

Add the following to your `gitlab-ci.yml`:

#### Java application or library

```yaml
include:
  - component: $CI_SERVER_FQDN/ohspl/oss/cicd-components/gradlew-releasing/java@0.0.0
    # set/override component inputs if needed
    inputs:
      java_version: "17"
```

#### Spring Boot Application

```yaml
include:
  - component: $CI_SERVER_FQDN/ohspl/oss/cicd-components/gradlew-releasing/springboot@0.0.0
    # set/override component inputs if needed
    inputs:
      java_version: "17"
```

## Workflow / Branching models

GitLab CI rules are configured to accommodate various branching models. Also, commits with conventional commit types like 'docs' are
excluded.

## License

This project is licensed under the Apache License 2.0 - see the [LICENSE](./LICENSE) file for details.
