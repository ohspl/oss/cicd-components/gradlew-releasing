# {{#environmentVariable}}CI_PROJECT_NAME{{/environmentVariable}} changelog

{{#releases}}
## [{{name}}]({{#environmentVariable}}CI_SERVER_URL{{/environmentVariable}}/{{#environmentVariable}}CI_PROJECT_NAMESPACE{{/environmentVariable}}/{{#environmentVariable}}CI_PROJECT_NAME{{/environmentVariable}}/-/tags/{{name}}) ({{date}})

{{#sections}}
### {{name}}

{{#commits}}
* [{{#short5}}{{sha}}{{/short5}}]({{#environmentVariable}}CI_SERVER_URL{{/environmentVariable}}/{{#environmentVariable}}CI_PROJECT_NAMESPACE{{/environmentVariable}}/{{#environmentVariable}}CI_PROJECT_NAME{{/environmentVariable}}/-/commit/{{sha}}) {{message.shortMessage}} ([{{authorAction.identity.name}}]({{#environmentVariable}}CI_SERVER_URL{{/environmentVariable}}/{{authorAction.identity.name}}))

{{/commits}}
{{^commits}}
No changes.
{{/commits}}
{{/sections}}
{{^sections}}
No changes.
{{/sections}}
{{/releases}}
{{^releases}}
No releases.
{{/releases}}
