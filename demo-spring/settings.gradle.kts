/*
	Typically, the cicd-gradle-plugin needs to be added as shown in the commented code.
	However, since the rootProject applies it in the settings scope, it is currently commented out.
 */

//pluginManagement {
//	repositories {
//		maven {
//			// name: O.W.S
//			// url : https://gitlab.com/ohspl
//			url = uri("https://gitlab.com/api/v4/groups/11708027/-/packages/maven")
//			credentials(HttpHeaderCredentials::class) {
//				val ciJobToken = System.getenv("CI_JOB_TOKEN")
//				val gitlabAccessToken: String? by settings
//				name = ciJobToken?.let { "Job-Token" } ?: "Private-Token"
//				value = ciJobToken ?: gitlabAccessToken
//			}
//			authentication {
//				create<HttpHeaderAuthentication>("header")
//			}
//			content {
//				includeGroupByRegex("org\\.gitlab\\.bullshit\\.devops\\.toolchain.*")
//			}
//		}
//		gradlePluginPortal()
//	}
//}
//
rootProject.name = "demo-spring"
//
//plugins {
//	id("org.gitlab.bullshit.devops.toolchain.cicd-gradle-plugin") version "latest.release
//}
