import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("java-library")
	id("maven-publish")
	id("org.springframework.boot")
	id("io.spring.dependency-management")
	kotlin("jvm")
	kotlin("plugin.spring") version "2.1.10"
	id("org.gitlab.bullshit.devops.toolchain.cicd-gradle-plugin")
}

group = "com.example"

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs += "-Xjsr305=strict"
		jvmTarget = java.sourceCompatibility.toString()
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

// region SonarCloud
// See: https://docs.sonarcloud.io/advanced-setup/ci-based-analysis/sonarscanner-for-gradle/
sonarqube {
	properties {
		//property("sonar.branch.name", System.getenv("CI_COMMIT_REF_NAME"))
		property("sonar.projectKey", "owsploos_gradlew-component-demo-spring")
		property("sonar.organization", "owsploos")
	}
}

tasks.named("sonar") {
	when {
		!System.getenv("CI").toBoolean() -> {
			dependsOn("check")
		}
	}
	// you can run sonar with gradle in CI by uncommented the next code line
	// or with as a CI/CD job with sonar-scanner-cli
	//  onlyIf { System.getenv("CI").toBoolean() }
} // endregion SonarCloud