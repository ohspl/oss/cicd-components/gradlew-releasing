import groovy.json.JsonSlurper
import java.io.IOException
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse


// to set project version in demo-spring subproject
// https://mooltiverse.github.io/nyx/guide/user/introduction/usage/#multi-project-builds
subprojects {
	version = rootProject.version
}

tasks.register("component-test") {
	group = "other"
	description = "Test if component pipeline run was success full"
	doLast {
		val logPrefix = "> component-test:"

		val apiURL = System.getenv("CI_API_V4_URL") ?: "https://gitlab.com/api/v4"
		val projectID = System.getenv("CI_PROJECT_ID") ?: "-1"

		val pipelineID = System.getenv("CI_PIPELINE_ID") ?: "-1"
		val route = java.net.URI.create("$apiURL/projects/$projectID/pipelines/$pipelineID/jobs")

		// job token cannot acccess pipeline endpoint
		val gitlabToken = System.getenv("GITLAB_TOKEN") ?: System.getenv("GL_TOKEN") ?: null
		val gitlabAccessToken = project.findProperty("gitlabAccessToken") as String?
		val accessToken = gitlabToken ?: gitlabAccessToken

		if (accessToken.isNullOrBlank()) {
			logger.warn(
				"""
				$logPrefix No GitLab token found. Please add a GitLab personal access token named
				"gitlabAccessToken" to your environment, for example to ~/.gradle/gradle.properties.""".trimIndent()
			)

			throw IllegalArgumentException("no access token provided for Task component-test")
		}

		val client = HttpClient.newHttpClient()
		val getPipelineUrlConnection = HttpRequest.newBuilder()
			.uri(route)
			// https://docs.gitlab.com/ee/api/rest/#personalprojectgroup-access-tokens
			.header("Authorization", "Bearer $accessToken")
			.build()
		logger.info("{} fetching data from {}", logPrefix, route)
		val response = client.send(getPipelineUrlConnection, HttpResponse.BodyHandlers.ofString())

		logger.debug("{} status code: ${response.statusCode()}", logPrefix)

		val result = if (response.statusCode() == 200) {
			JsonSlurper().parseText(response.body()) as List<Map<String, String>>

		} else {
			throw IOException("Wrong response code ${response.statusCode()} received")
		}

		var checkJobStatus = "failed"
		var checkAssembleStatus = "failed"
		var checkSonarStatus = "failed"

		for (jobMap in result) {
			val name = jobMap["name"] as String
			val status = jobMap["status"] as String
			logger.debug("{} name: $name status: $status", logPrefix)

			if (name.startsWith("check")) {
				checkJobStatus = status
			} else if (name.startsWith("assemble")) {
				checkAssembleStatus = status
			} else if (name.startsWith("sonar-check")) {
				checkSonarStatus = status
			}
		}

		if (!listOf(checkJobStatus, checkAssembleStatus).all { it == "success" }) {
			throw Exception("Test failed. because [check: $checkAssembleStatus, assemble: $checkAssembleStatus, sonar-check: $checkSonarStatus]")
		}
		logger.quiet("{} component-test was successful", logPrefix)
	}
}
